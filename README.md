# Word常用方法整理

## 1. 扩展类NiceXWPFDocument 

```java
public class NiceXWPFDocument extends XWPFDocument
```

### a. 四种常见word TABLE宽度（单位cm）

```java
/**
 * 通用边距的表格宽度：A4(20.99*29.6),页边距为3.17*2.54
 */
public static final float WIDTH_A4_FULL = 14.65f;
/**
 * 窄边距的表格宽度：A4(20.99*29.6),页边距为1.27*1.27
 */
public static final float WIDTH_A4_NARROW_FULL = 18.45f;
/**
 * 适中边距的表格宽度：A4(20.99*29.6),页边距为1.91*2.54
 */
public static final float WIDTH_A4_MEDIUM_FULL = 17.17f;
/**
 * 宽边距的表格宽度：A4(20.99*29.6),页边距为5.08*2.54
 */
public static final float WIDTH_A4_EXTEND_FULL = 10.83f;
```

![](D:\快照\20190508151644.png)

### b. 插入表格

在某个段落处添加段落

```java
public XWPFParagraph insertNewParagraph(XWPFRun run)
```

在某个文本处添加table

```java
public XWPFTable insertNewTable(XWPFRun run, int row, int col)
```

在某个cell处添加table

```java
public XWPFTable insertNewTable(XWPFTableCell cell, int row, int col)
```

在某个cell处添加table

```java
public XWPFTable insertNewTable(XWPFTableCell cell, String jsonStr)
```

## 2. Table相关方法

合并行单元格

```java
/**
 * 合并行单元格
 *
 * @param table
 *            表格对象
 * @param row
 *            行 从0开始
 * @param fromCol
 *            起始列
 * @param toCol
 *            结束列
 */
public static void mergeCellsHorizonal(XWPFTable table, int row, int fromCol, int toCol)
```

合并列单元格

```java
/**
 * 合并列单元格
 *
 * @param table
 *            表格对象
 * @param col
 *            列 从0开始
 * @param fromRow
 *            起始行
 * @param toRow
 *            结束行
 */
public static void mergeCellsVertically(XWPFTable table, int col, int fromRow, int toRow)
```

### 表格的宽度怎么定义的[^1]

是一个点的二十分之一，或者是1440分之一英寸官方解释如下：

> dxa - 指定该值为点的二十分之一（1/1440英寸）。
> 首先1英寸= 2.54厘米，A4纸大小为21cm * 29.7cm。
> 如果这个宽度设置成5670，则表示这个表格的宽度是10厘米。

抛开对这个单位理解的难度，我们最常见的应该是宽度自适应和宽度最大。
如果在POI-TL中设置了宽度= 0，则表格是宽度自适应的。
以A4纸为例，页面宽度为21cm，左右页边距各位3.17cm，则表格的宽度大约为
$$
（21 - 3.17*2）/ 2.54 * 1440 = 8310
$$
1厘米约等于567

设置表格每列宽度（指定每列的宽度厘米数）

```java
/**
 * 设置表格每列的宽度
 *
 * @param table
 *            表格对象
 * @param colWidths
 *            每列的宽度，单位CM
 */
@SuppressWarnings("unused")
// TODO
public static void widthTable(XWPFTable table, float[] colWidths)
```

设置表格每列宽度 (指定每列的宽度百分比)

```java
/**
 * 设置表格每列的宽度
 *
 * @param table
 *            表格对象
 * @param colPercents
 *            每列的百分比
 */
@SuppressWarnings("unused")
// TODO
public static void widthTable(XWPFTable table, float widthCM, double[] colPercents)
```

设置表格每列宽度，每列平均分布 （指定列数）

```java
/**
 * 表格设置宽度，每列平均分布
 *
 * @param table
 * @param widthCM
 * @param cols
 */
public static void widthTable(XWPFTable table, float widthCM, int cols)
```

设置表格宽度（为公式转化后的宽度，单位不是cm）

```java
/**
 * 设置表格总宽度
 * @param table
 * @param width
 */
public static void widthTable(XWPFTable table, BigInteger width)
```

设置cell内容

```java
public static void setCellText(XWPFTableCell cell, String text) 
```

获取单元格第一个Paragraph

```java
/**
 * 得到单元格第一个Paragraph
 *
 * @param cell
 * @return XWPFParagraph
 */
public static XWPFParagraph getCellFirstParagraph(XWPFTableCell cell)
```

 获取cell根据行列位置

```java
/**
 * 根据行列位置得到cell
 *
 * @param table
 * @param rowPosition
 * @param colPosition
 */
public static XWPFTableCell getCell(XWPFTable table, int rowPosition, int colPosition)
```

获取列宽（此处列宽不是cm）

```java
/**
 * 获取列宽
 *
 * @param table
 * @param colIndex
 * @return
 */
public static BigInteger getColWidth(XWPFTable table, int colIndex)
```

```java
/**
 * 设置表格总宽度
 * @param table
 * @param width
 */
public static void setTableWidth(XWPFTable table,BigInteger width)

```

## 3. 段落建造器

初始化

```java
public XWPFParagraphBuilder init(XWPFDocument document)

public XWPFParagraphBuilder init(XWPFParagraph paragraph)
```

属性

```java
//设置段落对齐方式
public XWPFParagraphBuilder align(ParagraphAlignment pAlign, TextAlignment vAlign) 
```

```java
//初始化段落间距属性，在设置各段落间距前调用
public XWPFParagraphBuilder initSpacing() 
```

```java
//设置段前和段后间距，以磅为单位
public XWPFParagraphBuilder spaceInPound(double before, double after) 
```

```java
//设置段前和段后间距，以行为单位
public XWPFParagraphBuilder spaceInLine(double beforeLines, double afterLines)
```

```java
//设置段落行距
public XWPFParagraphBuilder lineSpace(double value, STLineSpacingRule.Enum spaceRule)
```

```java
//设置段落缩进，以厘米为单位; 悬挂缩进高于首行缩进；右侧缩进高于左侧缩进
public XWPFParagraphBuilder indentInCM(double firstLine, double hanging, double right, double left)
```

```java
//设置段落缩进，以字符为单位; 悬挂缩进高于首行缩进；右侧缩进高于左侧缩进
public XWPFParagraphBuilder indentInChart(int firstLine, int hanging, int left, int right) 
```

## 4. 文本建造器

初始化

```java
public XWPFRunBuilder init(XWPFParagraph paragraph) 
    
public XWPFRunBuilder init(XWPFParagraph paragraph, boolean newLine)
    
public XWPFRunBuilder init(XWPFParagraph paragraph, int pos) 
    
public XWPFRunBuilder init(XWPFRun run)
```

属性

文字内容

```java
public XWPFRunBuilder content(String content)
```

```java
/**
 * 是否加粗
 *
 * @param bold
 * @return
 */
public XWPFRunBuilder bold(boolean bold)
```

```java
/**
 * 是否使用斜体
 * 
 * @param italic
 * @return
 */
public XWPFRunBuilder italic(boolean italic)
```

```java
/**
 * 是否使用删除线
 * 
 * @param strike
 * @return
 */
public XWPFRunBuilder strike(boolean strike)
```

```java
/**
 * 设置字体
 * 
 * @param cnFontFamily 
 * @param enFontFamily
 * @param fontSize
 * @return
 */
public XWPFRunBuilder font(String cnFontFamily, String enFontFamily, String fontSize)
```

```java
/**
 * 设置底纹
 * 
 * @param shdStyle
 * @param shdColor
 * @return
 */
public XWPFRunBuilder shade(STShd.Enum shdStyle, String shdColor)
```

```java
/**
 * @param position 字符垂直方向上间距位置； >0：提升； <0：降低；=磅值*2
 * @return
 */
public XWPFRunBuilder position(int position)
```

```java
/**
 * 设置字符间距信息
 * 
 * @param spacingValue
 * @return
 */
public XWPFRunBuilder space(int spacingValue)
```

```java
/**
 * @param verticalAlign SUPERSCRIPT：上标；SUBSCRIPT：下标
 * @return
 */
public XWPFRunBuilder verticalAlign(VerticalAlign verticalAlign)
```

```java
/**
 * 设置下划线
 * 
 * @param underStyle
 * @param underLineColor
 * @return
 */
public XWPFRunBuilder underLine(STUnderline.Enum underStyle, String underLineColor) 
```

```java
/**
 * 设置高亮(突出显示)
 * 
 * @param highStyle
 * @return
 */
public XWPFRunBuilder highLight(STHighlightColor.Enum highStyle) 
```

## 5. 其他

页眉页脚

```java
/**
 * @param document 文档
 * @Description: 页脚:显示页码信息
 */
public void simpleNumberFooter(XWPFDocument document) throws Exception

/**
 * @param document 文档
 * @Description: 页眉:显示时间信息
 */
public void simpleDateHeader(XWPFDocument document) throws Exception 
```

```java
/**
 * 增加自定义标题样式
 *
 * @param docxDocument 目标文档
 * @param strStyleId 样式名称
 * @param headingLevel 样式级别
 */
public void addCustomHeadingStyle(XWPFDocument docxDocument, String strStyleId, int headingLevel)
```

[^1]: POI-TL处理字表格（表）的最佳实践 http://deepoove.com/blog/#/posts/21

