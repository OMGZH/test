package com.demo.test.entity;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class Cover {
    private String headTitle;
    private List<CoverTitle> coverTitleList;

}
