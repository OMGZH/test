/*
 * Copyright 2014-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.demo.test.entity;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import com.demo.test.utils.TableTools;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTbl;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTblLayoutType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * 对原生poi的扩展
 *
 * @author Sayi
 * @version 0.0.1
 */
public class NiceXWPFDocument extends XWPFDocument {
    /**
     * 通用边距的表格宽度：A4(20.99*29.6),页边距为3.17*2.54
     */
    public static final float WIDTH_A4_FULL = 14.65f;
    /**
     * 窄边距的表格宽度：A4(20.99*29.6),页边距为1.27*1.27
     */
    public static final float WIDTH_A4_NARROW_FULL = 18.45f;
    /**
     * 适中边距的表格宽度：A4(20.99*29.6),页边距为1.91*2.54
     */
    public static final float WIDTH_A4_MEDIUM_FULL = 17.17f;
    /**
     * 宽边距的表格宽度：A4(20.99*29.6),页边距为5.08*2.54
     */
    public static final float WIDTH_A4_EXTEND_FULL = 10.83f;


    private static Logger logger = LoggerFactory.getLogger(NiceXWPFDocument.class);

    protected List<XWPFTable> allTables = new ArrayList<XWPFTable>();

    public NiceXWPFDocument() {
        super();
    }

    public NiceXWPFDocument(InputStream in) throws IOException {
        super(in);
        buildAllTables();
    }

    private void buildAllTables() {
        List<XWPFTable> tables = this.getTables();
        if (null == tables) return;
        else allTables.addAll(tables);

        List<XWPFTableRow> rows = null;
        List<XWPFTableCell> cells = null;
        List<XWPFTable> cellTables = null;
        for (XWPFTable table : tables) {
            rows = table.getRows();
            if (null == rows) continue;
            for (XWPFTableRow row : rows) {
                cells = row.getTableCells();
                if (null == cells) continue;
                for (XWPFTableCell cell : cells) {
                    cellTables = cell.getTables();
                    if (null != cellTables) allTables.addAll(cellTables);
                }
            }
        }
    }

    public XWPFTable getTableByCTTbl(CTTbl ctTbl) {
        for (int i = 0; i < allTables.size(); i++) {
            if (allTables.get(i).getCTTbl() == ctTbl) {
                return allTables.get(i);
            }
        }
        return null;
    }

    /**
     * 在某个段落起始处插入表格
     *
     * @param run
     * @param row
     * @param col
     * @return
     */
    public XWPFTable insertNewTable(XWPFRun run, int row, int col) {
        XmlCursor cursor = ((XWPFParagraph) run.getParent()).getCTP().newCursor();
        // XmlCursor cursor = run.getCTR().newCursor();
        if (isCursorInBody(cursor)) {
            String uri = CTTbl.type.getName().getNamespaceURI();
            String localPart = "tbl";
            cursor.beginElement(localPart, uri);
            cursor.toParent();

            CTTbl t = (CTTbl) cursor.getObject();
            XWPFTable newT = new XWPFTable(t, this, row, col);
            XmlObject o = null;
            while (!(o instanceof CTTbl) && (cursor.toPrevSibling())) {
                o = cursor.getObject();
            }
            if (!(o instanceof CTTbl)) {
                tables.add(0, newT);
            } else {
                int pos = tables.indexOf(getTable((CTTbl) o)) + 1;
                tables.add(pos, newT);
            }
            int i = 0;
            XmlCursor tableCursor = t.newCursor();
            try {
                cursor.toCursor(tableCursor);
                while (cursor.toPrevSibling()) {
                    o = cursor.getObject();
                    if (o instanceof CTP || o instanceof CTTbl) {
                        i++;
                    }
                }
                bodyElements.add(i > bodyElements.size() ? bodyElements.size() : i, newT);
                // bodyElements.add(i, newT);
                cursor.toCursor(tableCursor);
                cursor.toEndToken();

                //默认生成的table布局为FIXED
                TableTools.setTableLayout(newT, STTblLayoutType.FIXED);

                return newT;
            } finally {
                tableCursor.dispose();
            }
        }
        return null;
    }

    /**
     * 在某个cell起始处插入表格
     *
     * @param cell
     * @param row
     * @param col
     * @return
     */
    public XWPFTable insertNewTable(XWPFTableCell cell, int row, int col) {
        XWPFParagraph firstParagraph =  TableTools.getCellFirstParagraph(cell);

        if (CollUtil.isNotEmpty(firstParagraph.getRuns())) {
            return insertNewTable(cell.addParagraph().createRun(), row, col);
        } else {
            return insertNewTable(firstParagraph.createRun(), row, col);
        }
    }

    /**
     * 创建子表
     *
     * @param cell 单元格
     * @param jsonStr 子表的JSON字符串
     */
    public XWPFTable insertNewTable(XWPFTableCell cell, String jsonStr) {

        if (StringUtils.isBlank(jsonStr)) {
            throw new IllegalArgumentException("jsonStr 为空");
        }

        if (!JSONUtil.isJson(jsonStr)) {
            throw new IllegalArgumentException("jsonStr 不是合法的JSON字符串");
        }

        JSONArray rows = JSONUtil.parseArray(jsonStr);
        if (rows.isEmpty()) {
            throw new IllegalArgumentException("jsonStr 为空数组");
        }

        int rowsNum = rows.size();
        int[] colsNum = new int[rowsNum];
        for (int i = 0; i < rowsNum; i++) {
            JSONArray cols = rows.getJSONArray(i);
            colsNum[i] = cols.size();
        }

        int colMaxNum = NumberUtil.max(colsNum);

        XWPFTable parentTable = cell.getTableRow().getTable();
        XWPFTable childTable = insertNewTable(cell, rowsNum, colMaxNum);

        int colIndex = cell.getTableRow().getTableCells().indexOf(cell);

        //根据[[]]设值
        for (int i = 0; i < rowsNum; i++) {
            JSONArray cols = rows.getJSONArray(i);
            for (int j=0; j < cols.size(); j++) {
                XWPFTableCell cell1 = TableTools.getCell(childTable, i, j);
                TableTools.setCellText(cell1, cols.getStr(j));
            }

        }

        //设置列表宽度
        TableTools.widthTable(childTable, TableTools.getColWidth(parentTable, colIndex).subtract(BigInteger.valueOf(100)));

        return childTable;
    }

    /**
     * 在某个段落起始处插入段落
     *
     * @param run
     * @return
     */
    public XWPFParagraph insertNewParagraph(XWPFRun run) {
        // XmlCursor cursor = run.getCTR().newCursor();
        XmlCursor cursor = ((XWPFParagraph) run.getParent()).getCTP().newCursor();
        if (isCursorInBody(cursor)) {
            String uri = CTP.type.getName().getNamespaceURI();
            /*
             * TODO DO not use a coded constant, find the constant in the OOXML
             * classes instead, as the child of type CT_Paragraph is defined in
             * the OOXML schema as 'p'
             */
            String localPart = "p";
            // creates a new Paragraph, cursor is positioned inside the new
            // element
            cursor.beginElement(localPart, uri);
            // move the cursor to the START token to the paragraph just created
            cursor.toParent();
            CTP p = (CTP) cursor.getObject();
            XWPFParagraph newP = new XWPFParagraph(p, this);
            XmlObject o = null;
            /*
             * move the cursor to the previous element until a) the next
             * paragraph is found or b) all elements have been passed
             */
            while (!(o instanceof CTP) && (cursor.toPrevSibling())) {
                o = cursor.getObject();
            }
            /*
             * if the object that has been found is a) not a paragraph or b) is
             * the paragraph that has just been inserted, as the cursor in the
             * while loop above was not moved as there were no other siblings,
             * then the paragraph that was just inserted is the first paragraph
             * in the body. Otherwise, take the previous paragraph and calculate
             * the new index for the new paragraph.
             */
            if ((!(o instanceof CTP)) || (CTP) o == p) {
                paragraphs.add(0, newP);
            } else {
                int pos = paragraphs.indexOf(getParagraph((CTP) o)) + 1;
                paragraphs.add(pos, newP);
            }

            /*
             * create a new cursor, that points to the START token of the just
             * inserted paragraph
             */
            XmlCursor newParaPos = p.newCursor();
            try {
                /*
                 * Calculate the paragraphs index in the list of all body
                 * elements
                 */
                int i = 0;
                cursor.toCursor(newParaPos);
                while (cursor.toPrevSibling()) {
                    o = cursor.getObject();
                    if (o instanceof CTP || o instanceof CTTbl) i++;
                }
                bodyElements.add(i > bodyElements.size() ? bodyElements.size() : i, newP);
                cursor.toCursor(newParaPos);
                cursor.toEndToken();
                return newP;
            } finally {
                newParaPos.dispose();
            }
        }
        return null;
    }

    private boolean isCursorInBody(XmlCursor cursor) {
        XmlCursor verify = cursor.newCursor();
        verify.toParent();
        try {
            return true;// (verify.getObject() == this.getDocument().getBody());
        } finally {
            verify.dispose();
        }
    }

}