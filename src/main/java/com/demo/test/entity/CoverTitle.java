package com.demo.test.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CoverTitle {
    private String name;
    private String value;
}
