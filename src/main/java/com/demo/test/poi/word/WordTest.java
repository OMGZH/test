package com.demo.test.poi.word;

import com.demo.test.entity.Cover;
import com.demo.test.entity.CoverTitle;
import com.demo.test.entity.NiceXWPFDocument;
import com.demo.test.utils.OfficeUtil;
import com.demo.test.utils.TableTools;
import com.demo.test.utils.WordHolder;
import lombok.Cleanup;
import org.apache.poi.xwpf.usermodel.*;
import org.junit.Test;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STBorder;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STUnderline;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhangh 2019/5/5 22:13
 */
public class WordTest {
    @Test
    public void test() throws Exception {
        //Blank Document
        NiceXWPFDocument document = new NiceXWPFDocument();
        WordHolder wordHolder = new WordHolder();
        OfficeUtil officeUtil = new OfficeUtil();

        //        插入第一个表格
        XWPFTable table1 = document.insertNewTable(officeUtil.getOrAddParagraphFirstRun(document.createParagraph(), true, true), 2, 6);
        TableTools.widthTable(table1, NiceXWPFDocument.WIDTH_A4_FULL, 6);

        //        插入第二个表格
        XWPFTable table2 = document.insertNewTable(officeUtil.getOrAddParagraphFirstRun(document.createParagraph(), true, true), 2, 6);
        TableTools.widthTable(table2, NiceXWPFDocument.WIDTH_A4_NARROW_FULL, 6);

        //        插入第三个表格
        XWPFTable table3 = document.insertNewTable(officeUtil.getOrAddParagraphFirstRun(document.createParagraph(), true, true), 11, 3);
        TableTools.widthTable(table3, NiceXWPFDocument.WIDTH_A4_MEDIUM_FULL, new double[]{0.2, 0.3, 0.5});

        //        插入第四个表格
        XWPFTable table4 = document.insertNewTable(officeUtil.getOrAddParagraphFirstRun(document.createParagraph(), true, true), 3, 11);
        TableTools.widthTable(table4, NiceXWPFDocument.WIDTH_A4_EXTEND_FULL, 11);


        List<XWPFTableRow> row = table4.getRows();
        XWPFTableCell cell = row.get(1).getCell(3);

        wordHolder.getRunBuilder().init(TableTools.getCellFirstParagraph(cell)).content("美国队长")
                .font("华文行楷", "Times New Roman", "24")
                .bold(true).italic(false).strike(false).position(0).space(2);

        //        cell中插入子表
        XWPFTable s = document.insertNewTable(cell, "[[钢铁侠1,钢铁侠2,钢铁侠3],[2,4]]");

        for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 3; j++) {
                TableTools.setCellText(TableTools.getCell(table3, i, j), "蜘蛛侠" + i + j);
            }
        }

        TableTools.mergeCellsVertically(table3, 1, 2, 5);

        TableTools.borderTable(s, 1, STBorder.BASIC_THIN_LINES, TableRowAlign.CENTER);
        TableTools.borderTable(table1, 1, STBorder.BASIC_THIN_LINES, TableRowAlign.CENTER);
        TableTools.borderTable(table2, 1, STBorder.BASIC_THIN_LINES, TableRowAlign.CENTER);
        TableTools.borderTable(table3, 1, STBorder.BASIC_THIN_LINES, TableRowAlign.CENTER);
        TableTools.borderTable(table4, 1, STBorder.BASIC_THIN_LINES, TableRowAlign.CENTER);

        //插入页眉页脚
        officeUtil.simpleDateHeader(document);
        officeUtil.simpleNumberFooter(document);

        //Write the Document in file system
        @Cleanup FileOutputStream out = new FileOutputStream(new File("createdocument.docx"));
        document.write(out);
    }


    @Test
    public void testCover() throws IOException {
        List<CoverTitle> coverTitles = new ArrayList<>();
        coverTitles.add(CoverTitle.builder().name("灭霸").value("I 1 Y").build());
        coverTitles.add(CoverTitle.builder().name("黑寡妇").value("I 2 Y").build());
        coverTitles.add(CoverTitle.builder().name("鹰眼").value("I 3 Y").build());
        coverTitles.add(CoverTitle.builder().name("绿巨人").value("I 4 Y").build());
        coverTitles.add(CoverTitle.builder().name("蚁人").value("I 5 Y").build());
        coverTitles.add(CoverTitle.builder().name("钢铁侠").value("I 6 Y").build());
        coverTitles.add(CoverTitle.builder().name("美国队长").value("I 7 Y").build());

        Cover cover = Cover.builder().headTitle("复仇者联盟4：逆转未来").coverTitleList(coverTitles).build();

        WordHolder wordHolder = new WordHolder();
        OfficeUtil officeUtil = new OfficeUtil();

        //Blank Document
        NiceXWPFDocument document = new NiceXWPFDocument();
        XWPFTable table = document.createTable(coverTitles.size() + 1, 2);

        TableTools.borderTable(table, STBorder.NONE, TableRowAlign.CENTER);

        XWPFParagraph pLeft;
        XWPFParagraph pRight;

        TableTools.mergeCellsHorizonal(table, 0, 0, 1);
        XWPFTableCell headCell = TableTools.getCell(table, 0, 0);

        wordHolder.getRunBuilder().init(officeUtil.getCellFirstParagraph(headCell)).content(cover.getHeadTitle())
                .font("宋体", "Times New Roman", "32")
                .position(0).space(0).build();

        int row = 1;
        for (CoverTitle coverTitle : coverTitles) {
            XWPFTableCell leftCell = TableTools.getCell(table, row, 0);
            XWPFTableCell rightCell = TableTools.getCell(table, row, 1);

            pLeft = wordHolder.getParagraphBuilder().init(officeUtil.getCellFirstParagraph(leftCell)).align(ParagraphAlignment.CENTER,
                    TextAlignment.CENTER).build();

            pRight = wordHolder.getParagraphBuilder().init(officeUtil.getCellFirstParagraph(rightCell)).align(ParagraphAlignment.CENTER,
                    TextAlignment.CENTER).build();

            wordHolder.getRunBuilder().init(pLeft).content(coverTitle.getName() + "：")
                    .font("宋体", "Times New Roman", "32")
                    .position(0).space(0).build();

            wordHolder.getRunBuilder().init(pRight).content(coverTitle.getValue())
                    .underLine(STUnderline.THICK, "000000")
                    .font("宋体", "Times New Roman", "32")
                    .position(0).space(0).underLine(STUnderline.THICK, "000000").build();

            row++;
        }

        //Write the Document in file system
        @Cleanup FileOutputStream out = new FileOutputStream(new File("test-cover.docx"));
        document.write(out);

    }

}
